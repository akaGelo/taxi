package ru.vyukov.srg.pojo;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ru.vyukov.srg.domain.DriverStatus;

public class OrderNotificationTest {

	@Test
	public void testGetSendDelay() throws Exception {
		OrderNotification orderNotification = OrderNotification.createDemoInstance();
		long goldDelay = orderNotification.getSendDelay(DriverStatus.GOLD);

		assertBetween(goldDelay, 0, 0);

		long silverDelay = orderNotification.getSendDelay(DriverStatus.SILVER);
		assertBetween(silverDelay, SECONDS.toMillis(14), SECONDS.toMillis(16));

		long otherDelay = orderNotification.getSendDelay(DriverStatus.OTHER);
		assertBetween(otherDelay, SECONDS.toMillis(29), SECONDS.toMillis(31));
	}

	/**
	 * Используем примерные значения, так как тест зависит от текущего времени
	 * 
	 * @param val
	 * @param min
	 * @param max
	 */
	private void assertBetween(long val, long min, long max) {
		assertTrue(val >= min);
		assertTrue(val <= max);
	}
}
