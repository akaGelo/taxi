#!/bin/sh


if [ -z "$MEMORY_LIMIT" ]; then
    echo "MEMORY_LIMIT variable not set"
    exit 2
fi

java -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp -XX:OnOutOfMemoryError="kill -9 %p"  -Xmx${MEMORY_LIMIT}m -Dspring.profiles.active=production -Djava.security.egd=file:/dev/./urandom -cp app org.springframework.boot.loader.JarLauncher
