package ru.vyukov.srg.receiver;

import static ru.vyukov.srg.config.WebSocketConfig.EP_GOLD;
import static ru.vyukov.srg.config.WebSocketConfig.EP_OTHER;
import static ru.vyukov.srg.config.WebSocketConfig.EP_SILVER;
import static ru.vyukov.srg.domain.DriverStatus.OTHER;
import static ru.vyukov.srg.domain.DriverStatus.SILVER;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import ru.vyukov.srg.config.RabbitMqConfig;
import ru.vyukov.srg.domain.DriverStatus;
import ru.vyukov.srg.pojo.OrderNotification;

@Component
public class OrderNotificationReceiver {

	@Autowired
	private SimpMessageSendingOperations messagingTemplate;

	@RabbitListener(queues = RabbitMqConfig.GOLD_NOTIFICATIONS_QUEUE)
	public void goldReceive(@Payload OrderNotification notification) {
		// пересылаем без задержек
		messagingTemplate.convertAndSend(EP_GOLD, notification);
	}

	@RabbitListener(queues = RabbitMqConfig.SILVER_NOTIFICATIONS_QUEUE)
	public void silverReceive(@Payload OrderNotification notification) {
		delayedSending(notification, EP_SILVER,SILVER);
	}

	@RabbitListener(queues = RabbitMqConfig.OTHER_NOTIFICATIONS_QUEUE)
	public void otherReceive(@Payload OrderNotification notification) {
		delayedSending(notification, EP_OTHER,OTHER);
	}

	
	
	/**
	 * Реализациия отправки с задержкой. Тормозит поток на время, которое необходимо выждать до отправки уведомления
	 * @param notification
	 * @param destination
	 * @param driverStatus
	 */
	private void delayedSending(OrderNotification notification, String destination,DriverStatus driverStatus) {
		
		//вычисляем задержку
		long sendDelay = notification.getSendDelay(driverStatus);
		
		try {
			Thread.sleep(sendDelay);
		} catch (InterruptedException e) {
			// скорее всего отправка уже не актуальна
			return;
		}

		messagingTemplate.convertAndSend(destination, notification);
	}

}
