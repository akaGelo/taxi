package ru.vyukov.srg.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
@EnableRabbit
public class RabbitMqConfig {

	public static final String DRIVER_NOTIFICATIONS_EXCHANGE = "driver_notifications_exchange";

	public static final String GOLD_NOTIFICATIONS_QUEUE = "gold_notifications_queue";

	public static final String SILVER_NOTIFICATIONS_QUEUE = "silver_notifications_queue";

	public static final String OTHER_NOTIFICATIONS_QUEUE = "other_notifications_queue";

	@Bean
	public FanoutExchange driverNotificationsExchange() {
		return new FanoutExchange(DRIVER_NOTIFICATIONS_EXCHANGE);
	}

	// ----------------------------------------------------------------------
	@Bean
	public Queue goldNotificationsQueue() {
		return new Queue(GOLD_NOTIFICATIONS_QUEUE);
	}

	@Bean
	public Binding goldBinding() {
		return BindingBuilder.bind(goldNotificationsQueue()).to(driverNotificationsExchange());
	}

	// ----------------------------------------------------------------------
	@Bean
	public Queue silverNotificationsQueue() {
		return new Queue(SILVER_NOTIFICATIONS_QUEUE);
	}

	@Bean
	public Binding silverBinding() {
		return BindingBuilder.bind(silverNotificationsQueue()).to(driverNotificationsExchange());
	}

	// ----------------------------------------------------------------------
	@Bean
	public Queue otherNotificationsQueue() {
		return new Queue(OTHER_NOTIFICATIONS_QUEUE);
	}

	@Bean
	public Binding otherBinding() {
		return BindingBuilder.bind(otherNotificationsQueue()).to(driverNotificationsExchange());
	}

	// ----------------------------------------------------------------------

	@Bean
	public MappingJackson2MessageConverter jackson2Converter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		return converter;
	}

	@Bean
	public MessageConverter jackson2JsonMessageConverter() {
		Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter();
		return converter;
	}

	@Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
		RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(jackson2JsonMessageConverter());
		return rabbitTemplate;
	}

	@Bean
	public DefaultMessageHandlerMethodFactory myHandlerMethodFactory() {
		DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
		factory.setMessageConverter(jackson2Converter());
		return factory;
	}

}