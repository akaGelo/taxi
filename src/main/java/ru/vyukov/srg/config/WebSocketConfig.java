package ru.vyukov.srg.config;

import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.support.AbstractSubscribableChannel;
import org.springframework.messaging.support.ExecutorSubscribableChannel;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurationSupport;

@Configuration
// @EnableWebSocketMessageBroker alternative
public class WebSocketConfig extends WebSocketMessageBrokerConfigurationSupport {

	/**
	 * случайное число, это тестовый проект
	 */
	private static final int MAX_THREADS = 256;

	public static final String NOTIFICATIONS = "/notifications";

	public static final String EP_GOLD = "/notifications/gold";
	public static final String EP_SILVER = "/notifications/silver";
	public static final String EP_OTHER = "/notifications/other";

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker(NOTIFICATIONS);
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(NOTIFICATIONS);
	}

	@Override
	public AbstractSubscribableChannel brokerChannel() {
		Executor executor = new ThreadPoolExecutor(0, MAX_THREADS, 60L, TimeUnit.SECONDS,
				new SynchronousQueue<Runnable>());
		ExecutorSubscribableChannel channel = new ExecutorSubscribableChannel(executor);
		return channel;
	}

}