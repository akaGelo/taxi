package ru.vyukov.srg.pojo;

import java.util.Date;
import java.util.UUID;

import lombok.Value;
import ru.vyukov.srg.domain.DriverStatus;

/**
 * Уведомление о заказе
 * 
 * @author gelo
 *
 */
@Value
public class OrderNotification {

	private UUID orderId;

	private Date timestamp;

	public static OrderNotification createDemoInstance() {
		OrderNotification notification = new OrderNotification(UUID.randomUUID(), new Date());
		return notification;
	}

	/**
	 * Возвращает время, которое нужно подождать перед отправкой для переданного
	 * статуса водителя
	 * 
	 * @param silver
	 * @return
	 */
	public long getSendDelay(DriverStatus silver) {
		long actualDelay = System.currentTimeMillis() - timestamp.getTime();
		return actualDelay < silver.getSendDelay() ? silver.getSendDelay() - actualDelay : 0;
	}

}
