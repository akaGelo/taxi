package ru.vyukov.srg.controller;

import static ru.vyukov.srg.config.RabbitMqConfig.DRIVER_NOTIFICATIONS_EXCHANGE;

import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.vyukov.srg.pojo.OrderNotification;

@RestController
public class DemoController {

	@Autowired
	private RabbitOperations rabbitOperations;

	@RequestMapping("/simulatesend")
	public void simulateSend() {
		OrderNotification notification = OrderNotification.createDemoInstance();

		rabbitOperations.convertAndSend(DRIVER_NOTIFICATIONS_EXCHANGE, null, notification);
	}
}
