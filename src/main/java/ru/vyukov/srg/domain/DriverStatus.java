package ru.vyukov.srg.domain;

import java.util.concurrent.TimeUnit;

public enum DriverStatus {

	GOLD(0), SILVER(15), OTHER(30);

	private long sendDelay;

	private DriverStatus(long sendDelay) {
		this.sendDelay = TimeUnit.SECONDS.toMillis(sendDelay);
	}

	
	public long getSendDelay() {
		return sendDelay;
	}
}
